/*
 * DRRForest.h
 *
 *  Created on: 19 Dec 2012
 *      Author: aly
 */

#ifndef DRRFOREST_H_
#define DRRFOREST_H_

#include <vector>
#include "DiscriminativeRandomRegressionNode.h"
#include <boost/filesystem.hpp>
#include "RandomTree.h"
#include "DiscriminativeRandomRegressionNode.h"
#include "DRRClassSplitCalculator.h"
#include "PatchGenerator.h"
#include <pcl/point_types.h>


class DRRForest {
public:
	DRRForest(int number_of_trees, int max_depth, boost::filesystem::path& gt_pose_dir, 
			   boost::filesystem::path& depth_dir, boost::filesystem::path& mask_dir);
	
	const static int depth_channel =0;
	const static int normal_x_channel = 1;
	const static int normal_y_channel = 2;
	const static int normal_z_channel = 3;

	const static int max_channel_number = 0;

	void test(cv::Mat& test_img, cv::Mat& out);
	
	virtual ~DRRForest();
	
private:
	int number_of_trees;
	int max_depth;
	std::vector<DRRFunctor> functor_cache;
	std::vector<RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode> > trees;
	
	std::vector<DRRFunctor> generate_functors(int number_of_functors, 
										   int image_patch_height, int image_patch_width, 
										   int max_box_height, int max_box_width,
										   boost::shared_ptr<img_matrix> image_cache);
	
	int getRandomChannel();
	Rectangle generateRandomRectangle(int patch_height, int patch_width, int max_rec_height, int max_rec_width);
	//takes in a copy of training_data_points (note no &) so the get_and_remove will only effect local copy
	std::vector<std::vector<ImagePatch> >split_training_data_points(std::vector<ImagePatch> training_patches, int number_of_splits);
	boost::shared_ptr<img_matrix> createImageCache(PatchGenerator& pg);
	cv::Mat createIntegralImage(cv::Mat& depth_image, int type);
	
	void train(PatchGenerator& pg);
	
	int get_class(RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode>& tree, 
					int patch_centre_row, int patch_centre_col, int depth_file_key, pcl::PointXY& centre);
	int calculate_most_likely_class(int patch_centre_row, int patch_centre_col, int depth_file_key, pcl::PointXY& out_centre);

	
};

#endif /* DRRFOREST_H_ */
