/*
 * DiscriminativeRandomRegressionNode.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef DISCRIMINATIVERANDOMREGRESSIONNODE_H_
#define DISCRIMINATIVERANDOMREGRESSIONNODE_H_
#include <vector>
#include "ImagePatch.h"
#include <boost/shared_ptr.hpp>
#include <opencv2/opencv.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>


typedef boost::numeric::ublas::matrix<cv::Mat> img_matrix;

struct Rectangle{
	int top_left_x_offset_from_centre;
	int top_left_y_offset_from_centre;
	int height;
	int width;
	int area;
	Rectangle(){};
	Rectangle(int tlx_from_centre, int tly_from_centre, int h, int w):top_left_x_offset_from_centre(tlx_from_centre), top_left_y_offset_from_centre(tly_from_centre), height(h), width(w), area(height*width){};
};


struct DRRFunction{
   typedef double (*function_ptr)(const int& patch_centre_x, const int& patch_centre_y, 
		   	   	   	   	   	   	   Rectangle& rec1, Rectangle& rec2, const int& channel, const int& image_hash_key,
		   	   	   	   	   	   	   boost::shared_ptr<img_matrix> image_cache);
};

struct DRRFunctor{

private:
	boost::shared_ptr<img_matrix> image_cache;
	DRRFunction::function_ptr function;
	int functor_id;
	Rectangle rec1;
	Rectangle rec2;
	int channel;
//	static double max_variance = 400;
	
public:
	DRRFunctor(){};
	DRRFunctor(boost::shared_ptr<img_matrix> cache, DRRFunction::function_ptr function,int functor_id, Rectangle rec1, Rectangle rec2, int channel)
	:image_cache(cache), function(function), functor_id(functor_id), rec1(rec1), rec2(rec2), channel(channel)
	{}
	
	int get_functor_id(){
		return functor_id;
	}
	
	void set_functor_id(int f_id){
		functor_id = f_id;
	}

	double operator()(ImagePatch& data_point){
		
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		
		return function(data_point.get_patch_centre_x(), data_point.get_patch_centre_y(), rec1, rec2, channel, data_point.get_depth_file_key(), image_cache);
	}
	
	//This version has been put in for test time for speed, during training the generic random tree class uses the version
	//above
	double operator()(int patch_centre_x, int patch_centre_y, int depth_file_key){
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		return function(patch_centre_x, patch_centre_y, rec1, rec2, channel, depth_file_key, image_cache);
	}
	
	void set_image_cache(boost::shared_ptr<img_matrix> cache ){
		image_cache = cache;	
	}
	
	void replace_image_cache(boost::shared_ptr<img_matrix> cache){
		image_cache.reset();
		set_image_cache(cache);
	}
	
	boost::shared_ptr<img_matrix> get_image_cache(){
		return image_cache;
	}
};

class DiscriminativeRandomRegressionNode {

private:
	static const double MIN_PROPORTION_POSITIVE_CLASS = 0.5;
	static const double MAX_VARIANCE = 1500;
	
	boost::shared_ptr<DiscriminativeRandomRegressionNode> left;
	boost::shared_ptr<DiscriminativeRandomRegressionNode> right;
	double threshold;
	int functor_id;
	bool is_a_leaf;
	
	
	boost::numeric::ublas::matrix<double> head_offset_covariance_mat;
	boost::numeric::ublas::matrix<double> head_angle_covariance_mat;
	boost::array<double, 3> head_offset_means;
	boost::array<double, 3> head_angle_means;
	double total_variance;
	bool is_positive_class;
	bool use_leaf;
	
public:
	DiscriminativeRandomRegressionNode();
	
	boost::array<double, 3> get_head_offset_means(){
		return head_offset_means;
	}
	
	boost::array<double, 3> get_pose_angle_means(){
		return head_angle_means;
	}
	
	int get_functor_id(){
		return functor_id;
	}
	
	double get_threshold(){
		return threshold;
	}
	
	bool should_use_leaf(){
		return use_leaf;
	}
	
	void mark_as_leaf(){
		is_a_leaf = true;
	}
	
	bool is_leaf(){
		return is_a_leaf;
	}
	
	bool is_positive(){
		return is_positive_class;
	}
	
	void set_functor(DRRFunctor& functor){
		functor_id = functor.get_functor_id();
	}
	void set_threshold(double threshold){
		this->threshold = threshold;
	}
	
	void create_left_child(){
		left = boost::shared_ptr<DiscriminativeRandomRegressionNode>(new DiscriminativeRandomRegressionNode());
		
	}
	
	void create_right_child(){
		right = boost::shared_ptr<DiscriminativeRandomRegressionNode>(new DiscriminativeRandomRegressionNode());
	}
	
	boost::shared_ptr<DiscriminativeRandomRegressionNode> get_left_child(){
		return left;
	}
	
	boost::shared_ptr<DiscriminativeRandomRegressionNode> get_right_child(){
		return right;
	}
	
	void set_data(std::vector<ImagePatch>& data);
	
	~DiscriminativeRandomRegressionNode(){};
private:
	double calculate_proportion_of_positive_classes(std::vector<ImagePatch>& data, std::vector<ImagePatch>& out_positive_points);
};

#endif /* DISCRIMINATIVERANDOMREGRESSIONNODE_H_ */
