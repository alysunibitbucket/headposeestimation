/*
 * DiscriminativeRandomRegressionNode.cpp
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#include "DiscriminativeRandomRegressionNode.h"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_expression.hpp>
#include "utils.hpp"
#include "ImagePatch.h"
#include <vector>
#include "PatchGenerator.h"

using namespace std;
using namespace boost::numeric::ublas;


DiscriminativeRandomRegressionNode::DiscriminativeRandomRegressionNode():threshold(0), functor_id(0), is_a_leaf(false), is_positive_class(false), use_leaf(false){};

double DiscriminativeRandomRegressionNode::calculate_proportion_of_positive_classes(std::vector<ImagePatch>& data, std::vector<ImagePatch>& out_positive_points){
	double positive = 0;
	double total = data.size();
	for(std::vector<ImagePatch>::iterator it = data.begin(); it != data.end(); it++){
		if(it->get_class() == PatchGenerator::positive_class){
			out_positive_points.push_back(*it);
			positive++;
		}
	}
	
	return positive/total;
}

void DiscriminativeRandomRegressionNode::set_data(std::vector<ImagePatch>& data){
	std::vector<ImagePatch> positive_points;
	double positive_proportion = calculate_proportion_of_positive_classes(data, positive_points);
	if(positive_proportion >= MIN_PROPORTION_POSITIVE_CLASS){
		is_positive_class = true;
	}
	else{
		is_positive_class = false;
	}
	
	if(is_positive_class){
		head_offset_covariance_mat = calculate_offset_vector_covariance_matrix(positive_points);
		head_angle_covariance_mat = calculate_angle_vector_covariance_matrix(positive_points);
		head_offset_means = calculate_offset_vector_sample_means(positive_points);
		head_angle_means = calculate_angle_vector_sample_means(positive_points);
		total_variance = trace(head_offset_covariance_mat)  + trace(head_angle_covariance_mat);
	
		if(total_variance <= MAX_VARIANCE){
			use_leaf = true;
		}
		else{
			use_leaf = false;
		}
		
	}
	
  
}
