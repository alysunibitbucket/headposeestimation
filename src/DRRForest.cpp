/*
 * DRRForest.cpp
 *
 *  Created on: 19 Dec 2012
 *      Author: aly
 */

#include "DRRForest.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include "PatchGenerator.h"
#include "utils.hpp"
#include "RandomTree.h"
#include "DiscriminativeRandomRegressionNode.h"
#include "DRRClassSplitCalculator.h"
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/unordered_map.hpp>
#include "utils.hpp"
#include <boost/filesystem.hpp>
#include "PatchGenerator.h"
#include "pcl/point_types.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost;
using namespace cv;

double drr_rectangle_function(const int& patch_centre_x, const int& patch_centre_y, 
	   	   	   	   Rectangle& rec1, Rectangle& rec2, const int& channel, const int& image_hash_key,
	   	   	   	   boost::shared_ptr<img_matrix> image_cache){


	
	Mat& integral_image = image_cache->at_element(image_hash_key, channel);
	
	
	
	int rec1_bottom_right_x = patch_centre_x + rec1.top_left_x_offset_from_centre + rec1.width;
	int rec1_bottom_right_y = patch_centre_y + rec1.top_left_y_offset_from_centre + rec1.height;
	int rec1_top_left_x = rec1_bottom_right_x - rec1.width;
	int rec1_top_left_y = rec1_bottom_right_y - rec1.height;
	//integral image: S(x,y) = i(x,y) - i(x - width,y) - i(x, y - height) + i(x-width, y-height) 
	int32_t rec1_sum = integral_image.at<int32_t>(rec1_bottom_right_y + 1, rec1_bottom_right_x + 1) 
					- integral_image.at<int32_t>(rec1_top_left_y + 1, rec1_bottom_right_x + 1)
					- integral_image.at<int32_t>(rec1_bottom_right_y + 1, rec1_top_left_x + 1)
					+ integral_image.at<int32_t>(rec1_top_left_y + 1, rec1_top_left_x + 1);
	
	
	
	int rec2_bottom_right_x = patch_centre_x + rec2.top_left_x_offset_from_centre + rec2.width;
	int rec2_bottom_right_y = patch_centre_y + rec2.top_left_y_offset_from_centre + rec2.height;
	int rec2_top_left_x = rec2_bottom_right_x - rec2.width;
	int rec2_top_left_y = rec2_bottom_right_y - rec2.height;
	
	
	int32_t rec2_sum = integral_image.at<int32_t>(rec2_bottom_right_y + 1, rec2_bottom_right_x + 1) 
						- integral_image.at<int32_t>(rec2_top_left_y + 1, rec2_bottom_right_x + 1)
						- integral_image.at<int32_t>(rec2_bottom_right_y + 1, rec2_top_left_x + 1)
						+ integral_image.at<int32_t>(rec2_top_left_y + 1, rec2_top_left_x + 1);


	
	double f1 = rec1_sum/((double)(rec1.area));
	double f2 = rec2_sum/((double)(rec2.area));
	
	return abs(f1 - f2);
}

DRRForest::DRRForest(int number_of_trees, int max_depth, path& gt_pose_dir, path& depth_dir, path& mask_dir)
						:number_of_trees(number_of_trees), max_depth(max_depth), functor_cache(vector<DRRFunctor>()) 
{
	PatchGenerator pg = PatchGenerator(gt_pose_dir, depth_dir, mask_dir, 100, 100, 15);
	train(pg);
}

Mat DRRForest::createIntegralImage(Mat& depth_image, int type){
	Mat integral_image = Mat(depth_image.rows + 1, depth_image.cols + 1, CV_32S);
	
	for(int j = 0; j < integral_image.cols; j++){
		integral_image.at<int32_t>(0, j) = 0;
	}
	for(int i = 0; i < integral_image.rows; i++){
		integral_image.at<int32_t>(i, 0) = 0;
	}
	
	//TODO:for now just do the depth channel
	for(int i = 1; i < integral_image.rows; i++){
		for(int j = 1; j < integral_image.cols; j++){
			//II(y,x) = image(y,x) + II(y-1, x) + II (y, x-1) - II(y-1, x-1)
			
			integral_image.at<int32_t>(i,j) = depth_image.at<int16_t>(i-1, j-1) 
											+ integral_image.at<int32_t>(i -1, j)
											+ integral_image.at<int32_t>(i, j-1) 
											- integral_image.at<int32_t>(i-1, j-1);
			
		}
	}
	return integral_image;
}
boost::shared_ptr<img_matrix> DRRForest::createImageCache(PatchGenerator& pg){
	unordered_map<string, int> depth_file_map = pg.getDepthFileKeys();
	int number_of_depth_images = pg.getNumberOfDetphFiles();
	boost::shared_ptr<img_matrix> image_cache = boost::shared_ptr<img_matrix>(new img_matrix(number_of_depth_images, max_channel_number + 1));
	
	
	for(unordered_map<string, int>::iterator it = depth_file_map.begin(); it != depth_file_map.end(); it++){
		Mat depth_img =  loadDepthImageCompressed(it->first);
		for(int i = 0; i <= max_channel_number; i++){
			image_cache->operator ()(it->second, i) = createIntegralImage(depth_img, i);
		}
	}
	
	return image_cache;
}


void DRRForest::train(PatchGenerator& pg){
	vector<ImagePatch> training_patches = pg.getTrainingPatches();

	boost::shared_ptr<img_matrix> image_cache = createImageCache(pg);
	cout << "generating functors" << endl;
	vector<DRRFunctor> functors = generate_functors(500, pg.getPatchHeight(), pg.getPatchWidth(), 40, 40, image_cache);
	cout << "finished generating functors" << endl;
	vector<vector<ImagePatch> > training_patch_splits = split_training_data_points(training_patches, number_of_trees);
	
	DRRClassSplitCalculator datasplit_calculator = DRRClassSplitCalculator(15, 15, 20);
	
	
	for(int i = 0; i < number_of_trees; i++){
		cout << "****************building tree " << i+1 << " of " << number_of_trees << "********************" << endl;
		vector<ImagePatch> patches_split = training_patch_splits.at(i);
		RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode> tree(patches_split, functors, max_depth, 20, datasplit_calculator);
		trees.push_back(tree);
	}
}




Rectangle DRRForest::generateRandomRectangle(int patch_height, int patch_width, int max_rec_height, int max_rec_width){
	int rec_height = get_random_number_in_range(1, max_rec_height);
	int rec_width = get_random_number_in_range(1, max_rec_width);
	
	int top_left_x_offset_from_centre = get_random_number_in_range(-1*(patch_width/2), patch_width - rec_width);
	int top_left_y_offset_from_centre = get_random_number_in_range(-1*(patch_height/2), patch_height - rec_height);
	
	Rectangle rec = Rectangle(top_left_x_offset_from_centre, top_left_y_offset_from_centre, rec_height, rec_width);
	return rec;
}

int DRRForest::getRandomChannel(){
	return get_random_number_in_range(0, DRRForest::max_channel_number);
}

std::vector<DRRFunctor> DRRForest::generate_functors(int number_of_functors, int image_patch_height, 
														int image_patch_width, int max_rec_height, 
														int max_rec_width, boost::shared_ptr<img_matrix> image_cache){
	//image cache has to be a vector of integral_image caches i.e. a vector of Mat arrays, where each index to the vector represents a channel
	//so access is cache[channel][image_key]
	vector<DRRFunctor> functors;
	
	functor_cache.resize(number_of_functors);

	int functor_id = 0;
	for(int i = 0; i < number_of_functors; i++){
		Rectangle rec1 = generateRandomRectangle(image_patch_height, image_patch_width, max_rec_height, max_rec_width);
		Rectangle rec2 = generateRandomRectangle(image_patch_height, image_patch_width, max_rec_height, max_rec_width);
		int channel = getRandomChannel();
		DRRFunctor f = DRRFunctor(image_cache,drr_rectangle_function, functor_id, rec1, rec2, channel);
		functor_cache[functor_id] = f;
		functors.push_back(f);
		functor_id++;
	}
	
	return functors;
}


//takes in a copy of training_data_points (note no &) so the get_and_remove will only effect local copy
vector<vector<ImagePatch> > DRRForest::split_training_data_points(vector<ImagePatch> training_patches, int number_of_splits){
	vector<vector<ImagePatch> > splits;

	int points_to_remove_per_split = training_patches.size() * 0.25;
	//so we have 75% of items in each one

	for(int i = 0; i < number_of_splits; i++){
		vector<ImagePatch> split = vector<ImagePatch>(training_patches);
		for(int j = 0; j < points_to_remove_per_split; j++){
			get_and_remove_random_item(split);
		}
		splits.push_back(split);
	}

	return splits;
}


int DRRForest::get_class(RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode>& tree, 
		int patch_centre_row, int patch_centre_col, int depth_file_key, pcl::PointXY& centre){
		
	boost::shared_ptr<DiscriminativeRandomRegressionNode> current_node = tree.get_root();
	
	while(!(current_node->is_leaf())){
		int functor_id = current_node->get_functor_id();
		double threshold = current_node->get_threshold();
		
		double value = functor_cache[functor_id].operator ()(patch_centre_col, patch_centre_row, depth_file_key);
		if(value <= threshold){
			current_node = current_node->get_left_child();
		}
		else{
			current_node = current_node->get_right_child();
		}
	}
	
	
	if(current_node->is_positive()){
		boost::array<double, 3> offset_means = current_node->get_head_offset_means();
		centre.x = offset_means[0];
		centre.y = offset_means[1];
		return PatchGenerator::positive_class;
	}
	else{
		return PatchGenerator::negative_class;
	}
}


int DRRForest::calculate_most_likely_class(int patch_centre_row, int patch_centre_col, int depth_file_key, pcl::PointXY& out_centre_offset){
	
	std::vector<pcl::PointXY> centres;
	
	double number_of_trees = trees.size();
	double positive_count = 0;
	for(vector<RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode> >::iterator it = trees.begin(); it != trees.end(); it++){
		pcl::PointXY centre;
		int tree_class = get_class(*it, patch_centre_row, patch_centre_col, depth_file_key, centre);
		if(tree_class == PatchGenerator::positive_class){
			positive_count ++;
			centres.push_back(centre);
		}
	}
	
	if(positive_count / number_of_trees >= 0.6 ){
		out_centre_offset.x = 0;
		out_centre_offset.y = 0;
		
		for(vector<pcl::PointXY>::iterator it = centres.begin(); it != centres.end(); it++){
			out_centre_offset.x += it->x;
			out_centre_offset.y += it->y;
		}
		
		if(centres.size() == 0){
			cout << " 0 centres size at point " << patch_centre_row << ", " << patch_centre_col << endl;
			exit(-1);
		}
		
		out_centre_offset.x /= centres.size();
		out_centre_offset.y /= centres.size();
		cout << " returning positive for " << patch_centre_row << ", " << patch_centre_col << endl;
		cout << " with offsets " << out_centre_offset.y << ", " << out_centre_offset.x << endl;
		return PatchGenerator::positive_class;
	}
	else{
		return PatchGenerator::negative_class;
	}
	
}

void DRRForest::test(Mat& test_img, Mat& out){
	
	//update functor image cache with new cache
	boost::shared_ptr<img_matrix> image_cache = boost::shared_ptr<img_matrix>(new img_matrix(1, max_channel_number + 1));
	Mat ext = test_img;
	copyMakeBorder(test_img, ext, 20, 20, 20, 20, BORDER_REPLICATE, 0);
	
	for(int i = 0; i <= max_channel_number; i++){
		image_cache->operator ()(0, i) = createIntegralImage(ext, i);
	}
	
	for(vector<DRRFunctor>::iterator it = functor_cache.begin(); it != functor_cache.end(); it++){
		it->set_image_cache(image_cache);
	}
	
	cout << "image cache and functors updated" << endl;

	for(int row = 0; row < test_img.rows; row++){
		for(int col = 0; col < test_img.cols; col++){

			if(test_img.at<int16_t>(row,col) > 0 ){
				
				pcl::PointXY out_centre;
				int class_type = calculate_most_likely_class(row + 20, col + 20, 0, out_centre);
				
				if(class_type == PatchGenerator::positive_class){
				
					out.at<Vec3b>(row, col)[0] = 255;
					out.at<Vec3b>(row, col)[1] = 0;
					out.at<Vec3b>(row, col)[2] = 0;
					
					int head_row = row + out_centre.y;
					int head_col = col + out_centre.x;
					if( head_row < out.rows &&  head_row >=0 && head_col < out.cols && head_col >=0){
						out.at<Vec3b>(head_row, head_col)[0] = 0;
						out.at<Vec3b>(head_row, head_col)[1] = 0;
						out.at<Vec3b>(head_row, head_col)[2] = 255;
					}
					else{
						cout << "skipping vote at " << row << ", " << col << " because offsetss are " << out_centre.y << ", " << out_centre.x << endl;
					}
					
				}
			}
		}
	}
}


DRRForest::~DRRForest() {
	// TODO Auto-generated destructor stub
}

