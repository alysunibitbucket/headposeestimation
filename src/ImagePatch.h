/*
 * ImagePatch.h
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */

#ifndef IMAGEPATCH_H_
#define IMAGEPATCH_H_

#include <pcl/point_types.h>

class ImagePatch {
private:
	int class_type;
	pcl::PointXYZ head_centre_point_offset_from_patch_centre;
	float pitch;
	float yaw;
	float roll;
	std::string person_id;
	int depth_file_key;
	pcl::PointXYZ patch_centre_point;
	
	
public:
	ImagePatch(int class_type, pcl::PointXYZ head_centre_point, float pitch, float yaw, float roll, 	std::string person_id, int depth_file_key, pcl::PointXYZ patch_centre_point);
	
	inline bool operator==(const ImagePatch& other) const{
		return other.class_type == this->class_type;
	}
	inline bool operator!=(const ImagePatch& other) const{
		return !(*this == other);
	}

	int get_class(){
		return class_type;
	}
	
	int get_patch_centre_x(){
		return patch_centre_point.x;
	}
	
	int get_patch_centre_y(){
		return patch_centre_point.y;
	}
	
	int get_depth_file_key(){
		return depth_file_key;
	}

	double get_centre_value(int i){
		switch(i){
			case 0: return head_centre_point_offset_from_patch_centre.x;break;
			case 1: return head_centre_point_offset_from_patch_centre.y;break;
			default: return head_centre_point_offset_from_patch_centre.z;
		}
	}
	
	double get_angle_value(int i){
		switch(i){
			case 0: return pitch; break;
			case 1: return yaw; break;
			default: return roll; 
		}
	}
	
	int get_head_centre_x(){
		return head_centre_point_offset_from_patch_centre.x;
	}
	
	int get_head_centre_y(){
		return head_centre_point_offset_from_patch_centre.y;
	}
	
	int get_head_centre_z(){
		return head_centre_point_offset_from_patch_centre.z;
	}
	
	float get_pitch(){
		return pitch;
	}
	
	float get_roll(){
		return roll;
	}
	
	float get_yaw(){
		return yaw;
	}
	
	~ImagePatch();
};

#endif /* IMAGEPATCH_H_ */
