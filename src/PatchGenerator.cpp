/*
 * PatchGenerator.cpp
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */

#include "PatchGenerator.h"
#include <boost/filesystem.hpp>
#include "utils.hpp"
#include <pcl/point_types.h>
#include <opencv2/opencv.hpp>
#include "ImagePatch.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost;
using namespace pcl;
using namespace cv;



string getFilename(const path& dir, const string& filename_id, const string& file_suffix){
	return dir.string() + "/" + filename_id + file_suffix;
}


PatchGenerator::PatchGenerator(path& gt_pose_dir, path& depth_dir, path& mask_dir, int patch_size_height, int patch_size_width, int number_of_positive_and_negative_patches) :
	gt_pose_dir(gt_pose_dir), depth_dir(depth_dir), mask_dir(mask_dir), patch_size_height(patch_size_height), 
	patch_size_width(patch_size_width), number_of_positive_and_negative_patches(number_of_positive_and_negative_patches)
{

	loadTrainingFiles();
	generateTrainingPatches();

}


void PatchGenerator::getPositiveAndNegativePixels(Mat& depth_img, const string& mask_filename, vector<PointXYZ>& positive, vector<PointXYZ>& negative){
	Mat mask_img = imread(mask_filename, CV_LOAD_IMAGE_GRAYSCALE);
	
	for(int row = 0; row < depth_img.rows; row++){
		for(int col = 0; col < depth_img.cols; col++){
			if(depth_img.at<int16_t>(row, col) > 0.0 && row + patch_size_height < depth_img.rows && col + patch_size_width < depth_img.cols){
				PointXYZ point;
				point.x = col;
				point.y = row;
				point.z = depth_img.at<int16_t>(row, col);
				
				if(mask_img.at<uchar>(row, col) > 0.0){
					positive.push_back(point);
				}
				else{
					negative.push_back(point);
				}
			}
		}
	}
}


void PatchGenerator::generateTrainingPatches(){
	training_patches = vector<ImagePatch>();
	for(vector<string>::iterator person_id = person_data_subdir_ids.begin(); person_id != person_data_subdir_ids.end(); person_id++){
		path mask_sub_dir = path(mask_dir.string() + "/" + *person_id);
		path pose_sub_dir = path(gt_pose_dir.string() + "/" + *person_id);
		path depth_sub_dir = path(depth_dir.string() + "/" +  *person_id);

		for(vector<string>::iterator filename_id = person_data_to_filename_ids[*person_id].begin(); filename_id != person_data_to_filename_ids[*person_id].end(); filename_id++){
			string mask_file = getFilename(mask_sub_dir, *filename_id, mask_file_suffix);
			string pose_file = getFilename(pose_sub_dir, *filename_id, pose_file_suffix);
			string depth_file = getFilename(depth_sub_dir, *filename_id, depth_file_suffix);
			vector<PointXYZ> positive_points;
			vector<PointXYZ> negative_points;

			PointXYZ head_centre_point;
			//{x,y,z,pitch,yaw,roll}			
			float* pose = readGroundTruthPose(pose_file);
			head_centre_point.x = pose[0];
			head_centre_point.y = pose[1];
			head_centre_point.z = pose[2];
			
			
			Mat depth_img = loadDepthImageCompressed(depth_file);
			getPositiveAndNegativePixels(depth_img, mask_file, positive_points, negative_points);
			
			int centre_row = depth_img.rows/2;
			int centre_col = depth_img.cols/2;
	
			head_centre_point.x += centre_col;
			head_centre_point.y += centre_row;
			
			if(number_of_positive_and_negative_patches > (int) positive_points.size() ||
				number_of_positive_and_negative_patches > (int) negative_points.size()){
				cerr << " not enough positive and negative points in image " << depth_file << " asked to sample " << number_of_positive_and_negative_patches << " points "
						<< " but have " << positive_points.size() << " positive and " << negative_points.size() << " negative " << " please choose a different number " << endl;
				exit(-1);
			}
			int depth_file_key = depth_file_keys[depth_file];
			for(int i = 0; i < number_of_positive_and_negative_patches; i++){
				
				PointXYZ positive_patch_centre_point = get_and_remove_random_item(positive_points);
				
				ImagePatch positive_patch = ImagePatch(positive_class, head_centre_point, pose[3], pose[4], pose[5], *person_id,  depth_file_key, positive_patch_centre_point);
				
				PointXYZ negative_patch_centre_point = get_and_remove_random_item(negative_points);
				ImagePatch negative_patch = ImagePatch(negative_class, head_centre_point, pose[3], pose[4], pose[5], *person_id, depth_file_key, negative_patch_centre_point);
				
				training_patches.push_back(positive_patch);
				training_patches.push_back(negative_patch);
			}
		}
	}
	
	
	
	
	
}




void PatchGenerator::generateFilenameIds() {
	
	directory_iterator end_itr; //default construction provides an end reference
	for (directory_iterator itr(gt_pose_dir); itr != end_itr; itr++) {
		person_data_subdir_ids.push_back(itr->path().filename().string());
	}

	for (vector<string>::iterator it = person_data_subdir_ids.begin();it != person_data_subdir_ids.end(); it++) {
		if (person_data_to_filename_ids.find(*it) == person_data_to_filename_ids.end()) {
			person_data_to_filename_ids[*it] = vector<string>();
		}

		path sub_dir = path(gt_pose_dir.string() + "/" + *it);
		directory_iterator end_itr; //default construction provides an end reference
		for (directory_iterator itr(sub_dir); itr != end_itr; itr++) {
			string filename = itr->path().filename().string();
			if (filename.find(pose_file_suffix)) {
				filename = filename.substr(0,filename.size() - pose_file_suffix.size());
				person_data_to_filename_ids[*it].push_back(filename);
			}
		}
	}
}

void PatchGenerator::loadDepthFiles(){
	int file_id = 0;
	for(vector<string>::iterator it = person_data_subdir_ids.begin(); it != person_data_subdir_ids.end(); it++){
		depth_files_map[*it] = vector<path>();
		depth_file_keys = unordered_map<string, int>();
		path sub_dir = path(depth_dir.string() + "/" + *it);
		for(vector<string>::iterator filename_id = person_data_to_filename_ids[*it].begin(); filename_id != person_data_to_filename_ids[*it].end(); filename_id++){
			path file = path(getFilename(sub_dir, *filename_id, depth_file_suffix));
			cout << "loading depth file " << file.string() << endl;
			depth_files_map[*it].push_back(file);
			depth_file_keys[file.string()] = file_id;
			file_id++;
		}
	}
	
	unordered_map<int, string> key_to_depth_file_map;
	
	for(int i = 0; i < file_id; i++){
		
	}
	
	
	number_of_depth_files = file_id;
}



void PatchGenerator::loadMaskFiles(){
	for(vector<string>::iterator it = person_data_subdir_ids.begin(); it != person_data_subdir_ids.end(); it++){
		mask_files_map[*it] = vector<path>();
		path sub_dir = path(mask_dir.string() + "/" + *it);
		for(vector<string>::iterator filename_id = person_data_to_filename_ids[*it].begin(); filename_id != person_data_to_filename_ids[*it].end(); filename_id++){
			path file = path(getFilename(sub_dir, *filename_id, mask_file_suffix));
			mask_files_map[*it].push_back(file);
		}
	}
}

void PatchGenerator::loadTrainingFiles() {
	generateFilenameIds();
	loadDepthFiles();
	loadMaskFiles();
}


vector<string> PatchGenerator::getPersonIds(){
	return person_data_subdir_ids;
}

int PatchGenerator::getNumberOfDetphFiles(){
	return number_of_depth_files;
}
	
vector<ImagePatch> PatchGenerator::getTrainingPatches(){
	return training_patches;
}

int PatchGenerator::getPatchHeight(){
	return patch_size_height;
}

int PatchGenerator::getPatchWidth(){
	return patch_size_width;
}

unordered_map<string, int > PatchGenerator::getDepthFileKeys(){
	return depth_file_keys;
}

PatchGenerator::~PatchGenerator() {
	// TODO Auto-generated destructor stub
}

