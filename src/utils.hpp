/*
 * utils.hpp
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/array.hpp>
#include "ImagePatch.h"

template <typename T> T get_random_item(const std::vector<T>& v){
	int index = rand()%v.size();
	return v.at(index);
}


template <typename T> T get_and_remove_random_item(std::vector<T>& v){
	//not perfectley uniformly distributed by using % but fast and as we dont need the perfect distribution for this task it will do
	int index = rand()%v.size();
	T elem = v.at(index);
	remove_at(v, index);
	return elem;
}

template <typename T> void remove_at(std::vector<T>& v, typename std::vector<T>::size_type index){
	std::swap(v[index], v.back());
	v.pop_back();
}

template <typename T> bool contains(std::vector<T>& v, T obj){
	return std::find(v.begin(), v.end(), obj) != v.end();
}

/*
 * returns a pair containing [value, data point] 
 */
template<typename DataPoint, typename RandomFunctor>
std::vector<std::pair<double,DataPoint> > compute_value_data_pairs(std::vector<DataPoint>& data, RandomFunctor& functor)
{
	std::vector<std::pair<double,DataPoint> > value_data_pairs;
	value_data_pairs.reserve(data.size());
	for(typename std::vector<DataPoint>::iterator data_point = data.begin(); data_point < data.end(); data_point++){
		
		double value = functor(*data_point);
		std::pair<double, DataPoint> value_data_pair =  std::pair<double, DataPoint>(value, *data_point);
		value_data_pairs.push_back(value_data_pair);
	}
	
	return value_data_pairs;
}

template<typename DataPoint>
double calculate_mean(std::vector<std::pair<double, DataPoint> >& value_data_pairs){
		double mean = 0;
		for(typename std::vector<std::pair<double,DataPoint> >::iterator x = value_data_pairs.begin(); x < value_data_pairs.end(); x++){
			mean += x->first;
		}
		
		return mean/((double)value_data_pairs.size());
	}

template<typename DataPoint>	
double calculate_standard_deviation(std::vector<std::pair<double, DataPoint> >& value_data_pairs, double& mean){
	double variance = 0;
	for(typename std::vector<std::pair<double, DataPoint> >::iterator x = value_data_pairs.begin(); x < value_data_pairs.end(); x++){
		variance += (x->first - mean ) * (x->first - mean);
	}
	variance /= (double)(value_data_pairs.size() - 1);
	return sqrt(variance);
}

template<typename DataPoint>
boost::random::normal_distribution<double> create_gaussian_from_samples(std::vector<std::pair<double,DataPoint> >& sample_value_weight_pairs){
		double mean = calculate_mean(sample_value_weight_pairs);
		double standard_deviation = calculate_standard_deviation(sample_value_weight_pairs, mean);
		return boost::random::normal_distribution<double>(mean, standard_deviation);
}



template<typename T> 
T trace(boost::numeric::ublas::matrix<T>& mat){
	boost::numeric::ublas::matrix_vector_range<boost::numeric::ublas::matrix<T> > diag(mat, boost::numeric::ublas::range (0,mat.size1()), boost::numeric::ublas::range (0,mat.size1())); 
	return boost::numeric::ublas::sum(diag); 
}


bool replace(std::string& str, const std::string& from, const std::string& to);
float* readGroundTruthPose(const std::string& filename);
cv::Mat loadDepthImageCompressed(const std::string& filename);
int get_random_number_in_range(int min, int max);
void showDepthImage(cv::Mat& depth_img);
int determinant_sign(const boost::numeric::ublas::permutation_matrix<std ::size_t>& pm);
double determinant( boost::numeric::ublas::matrix<double>& m );
 
boost::numeric::ublas::matrix<double> calculate_offset_vector_covariance_matrix(std::vector<ImagePatch>& data);
boost::numeric::ublas::matrix<double> calculate_angle_vector_covariance_matrix(std::vector<ImagePatch>& data);
boost::array<double, 3> calculate_offset_vector_sample_means(std::vector<ImagePatch>& data);
boost::array<double, 3> calculate_angle_vector_sample_means(std::vector<ImagePatch>& data);


#endif /* UTILS_HPP_ */
