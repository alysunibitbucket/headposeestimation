/*
 * HeadPose.cpp
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <pcl/point_types.h>
#include <boost/filesystem.hpp>
#include "PatchGenerator.h"
#include "utils.hpp"
#include "RandomTree.h"
#include "DiscriminativeRandomRegressionNode.h"
#include "DRRClassSplitCalculator.h"
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "DRRForest.h"

using namespace std;
using namespace cv;
using namespace pcl;
using namespace boost::filesystem;
using namespace boost::numeric::ublas;

Mat cii(Mat& depth_image){
	Mat integral_image = Mat(depth_image.rows + 1, depth_image.cols + 1, CV_32S);
	
	for(int j = 0; j < integral_image.cols; j++){
		integral_image.at<int32_t>(0, j) = 0;
	}
	for(int i = 0; i < integral_image.rows; i++){
		integral_image.at<int32_t>(i, 0) = 0;
	}
	
	//TODO:for now just do the depth channel
	for(int i = 1; i < integral_image.rows; i++){
		for(int j = 1; j < integral_image.cols; j++){
			//II(y,x) = image(y,x) + II(y-1, x) + II (y, x-1) - II(y-1, x-1)
			
			integral_image.at<int32_t>(i,j) = depth_image.at<int16_t>(i-1, j-1) 
											+ integral_image.at<int32_t>(i -1, j)
											+ integral_image.at<int32_t>(i, j-1) 
											- integral_image.at<int32_t>(i-1, j-1);
			
		}
	}
	return integral_image;
}

int main(int argc, char* argv[]) {
	
	path gt_pose_dir = path(argv[1]);
	path depth_dir = path(argv[2]);
	path mask_dir = path(argv[3]);
	if (!exists(gt_pose_dir) || !exists(depth_dir) || !exists(mask_dir)) {
		cerr << " directories do  not exist, exiting" << endl;
		exit(-1);
	}			
	

	string test_image_filename = "/home/aly/Datasets/Kinect_ETH_Mini/Test/Depth/01/frame_00035_depth.bin";
	
	Mat test_depth_img = loadDepthImageCompressed(test_image_filename);
	
	Mat out = Mat(test_depth_img.rows, test_depth_img.cols, CV_8UC3);
	namedWindow("window", CV_WINDOW_AUTOSIZE);
	for(int i = 0; i < out.rows; i++){
		for(int j = 0; j < out.cols; j++){
			out.at<Vec3b>(i,j)[0] = 0;
			out.at<Vec3b>(i,j)[1] = 0;
			out.at<Vec3b>(i,j)[2] = 0;
		}
	}
	
	cout << gt_pose_dir << endl;
	cout << depth_dir << endl;
	cout << mask_dir << endl;
//	
	DRRForest forest = DRRForest(7, 15, gt_pose_dir, depth_dir, mask_dir);
	
	cout << "begining test" << endl;
	forest.test(test_depth_img, out);
	
	namedWindow("window", CV_WINDOW_AUTOSIZE);
	imshow("window", out);
	
	waitKey(0);
	
	return 0;
}


