/*
 * DRRClassSplitCalculator.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef DRRCLASSSPLITCALCULATOR_H_
#define DRRCLASSSPLITCALCULATOR_H_
#include "RandomTree.h"
#include "ImagePatch.h"
#include "DiscriminativeRandomRegressionNode.h"
#include <vector>
#include "utils.hpp"
#include <boost/random/normal_distribution.hpp>
#include <boost/random.hpp>
#include <boost/shared_array.hpp>

class DRRClassSplitCalculator {
private:
	typedef boost::mt19937 rng;
	rng random_number_generator;
	int number_of_functors_to_try;
	int number_of_thresholds_to_try;
	int min_number_per_node;
	
	typedef boost::numeric::ublas::matrix<double> matrix;
	double calculate_classification_entropy(std::vector<ImagePatch>& left_split, std::vector<ImagePatch>& right_split);
	
	double calculate_current_regression_uncertainty(std::vector<ImagePatch>& data);
public:
	DRRClassSplitCalculator(int number_of_functors_to_try,int number_of_thresholds_to_try, int min_number_per_node );
	double calculate_regression_uncertainty(std::vector<ImagePatch>& left_split, std::vector<ImagePatch>& right_split);
	data_split<ImagePatch, DRRFunctor> getBestDataSplit(std::vector<ImagePatch>& data, std::vector<DRRFunctor>& functors, 
														 double current_depth);	
	virtual ~DRRClassSplitCalculator();
};

#endif /* DRRCLASSSPLITCALCULATOR_H_ */
