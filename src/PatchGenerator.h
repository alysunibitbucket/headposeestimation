/*
 * PatchGenerator.h
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */

#ifndef PATCHGENERATOR_H_
#define PATCHGENERATOR_H_
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include <pcl/point_types.h>
#include "ImagePatch.h"
#include <opencv2/opencv.hpp>

const std::string pose_file_suffix = "_pose.bin";
const std::string depth_file_suffix = "_depth.bin";
const std::string mask_file_suffix = "_depth_mask.png";

class PatchGenerator {

	
public:
static const int positive_class = 1;
static const int negative_class = 0;
	
private:
	boost::filesystem::path gt_pose_dir;
	boost::filesystem::path depth_dir;
	boost::filesystem::path mask_dir;
	int patch_size_height;
	int patch_size_width;
	int number_of_positive_and_negative_patches;
	int number_of_depth_files;
	std::vector<std::string> person_data_subdir_ids;
	
	boost::unordered_map<std::string, std::vector<std::string> > person_data_to_filename_ids;
	boost::unordered_map<std::string, std::vector<boost::filesystem::path> > mask_files_map;
	boost::unordered_map<std::string, std::vector<boost::filesystem::path> > depth_files_map;
	boost::unordered_map<std::string, int> depth_file_keys;
	
	std::vector<ImagePatch> training_patches;
	
	void loadTrainingFiles();
	void generateFilenameIds();
	void loadMaskFiles();
	void loadDepthFiles();
	
	void createImagePatches();
	void generateTrainingPatches();
	
	void getPositiveAndNegativePixels(cv::Mat& depth_img, const std::string& mask_filename, 
											std::vector<pcl::PointXYZ>& positive, std::vector<pcl::PointXYZ>& negative);
	
	
	
public:
	
	std::vector<ImagePatch> getTrainingPatches();
	PatchGenerator(boost::filesystem::path& gt_pose_dir, boost::filesystem::path& depth_dir, boost::filesystem::path& mask_dir, int patch_size_height, int patch_size_width, int number_of_positive_and_negative_patches);
	std::vector<std::string> getPersonIds();
	int getNumberOfDetphFiles();
	boost::unordered_map<std::string, int > getDepthFileKeys();
	int getPatchHeight();
	int getPatchWidth();
	
	virtual ~PatchGenerator();
};


//PatchGenerator::pose_file_suffix = "_pose.txt";
#endif /* PATCHGENERATOR_H_ */
