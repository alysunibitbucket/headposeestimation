/*
 * DRRClassSplitCalculator.cpp
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#include "DRRClassSplitCalculator.h"
#include <vector>
#include "DiscriminativeRandomRegressionNode.h"
#include "ImagePatch.h"
#include <boost/unordered_map.hpp>
#include "float.h"
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>

using namespace std;
using namespace boost::random;
using namespace boost;


DRRClassSplitCalculator::DRRClassSplitCalculator(int number_of_functors_to_try,int number_of_thresholds_to_try, int min_number_per_node)
: random_number_generator(rng(time(0))), number_of_functors_to_try(number_of_functors_to_try), number_of_thresholds_to_try(number_of_thresholds_to_try), min_number_per_node(min_number_per_node) {
}

data_split<ImagePatch, DRRFunctor> DRRClassSplitCalculator::getBestDataSplit(vector<ImagePatch>& data, vector<DRRFunctor>& functors, double current_depth)
{
	
	
	double current_node_regression_uncertainty = calculate_current_regression_uncertainty(data);
	
	
	typedef boost::random::normal_distribution<double> gaussian;
	typedef boost::variate_generator<rng&, gaussian > variate_generator;	
	
	data_split<ImagePatch, DRRFunctor> best_data_split;
	
	double best_value = -DBL_MAX;
	
	for(int i = 0; i < number_of_functors_to_try; i++){
		DRRFunctor functor = get_random_item(functors);
						
		vector<pair<double, ImagePatch> > value_data_pairs = compute_value_data_pairs(data, functor);
				
		gaussian distribution = create_gaussian_from_samples(value_data_pairs); 

		variate_generator var_gen = variate_generator(random_number_generator, distribution);
		vector<ImagePatch > left_split;
		vector<ImagePatch > right_split;		
		for(int j = 0; j < number_of_thresholds_to_try; j++){
		
			double threshold = var_gen();
			left_split.clear();
			right_split.clear();
			for(typename vector<pair<double, ImagePatch> >::iterator it = value_data_pairs.begin(); it < value_data_pairs.end(); it++){
				if(it->first <= threshold){
					left_split.push_back(it->second);
				}
				else{
					right_split.push_back(it->second);
				}
			}
			if(left_split.size() > min_number_per_node &&  right_split.size() > min_number_per_node){
				double regression_uncertainty = current_node_regression_uncertainty - calculate_regression_uncertainty(left_split, right_split); 
				double classification_entropy = calculate_classification_entropy(left_split, right_split);
			
				double value = classification_entropy + ((1.0 - exp(-1* (current_depth/1.0))) * regression_uncertainty);
//				double value = classification_entropy + 
				if(value > best_value){
					best_data_split = data_split<ImagePatch, DRRFunctor>(left_split, right_split, functor, threshold);
					best_value = value;
				}
			}
		}
	}
	return best_data_split;
}

double DRRClassSplitCalculator::calculate_classification_entropy(vector<ImagePatch>& left_split, vector<ImagePatch>& right_split){
	
	double total_points = left_split.size() + right_split.size();
	double p_head_given_left;
	
	double left_counts [2] = {0,0};
	double right_counts[2] = {0,0};
	
	for(vector<ImagePatch>::iterator it = left_split.begin(); it != left_split.end(); it++){
		left_counts[it->get_class()] += 1.0;
	}
	for(vector<ImagePatch>::iterator it = right_split.begin(); it != right_split.end(); it++){
		right_counts[it->get_class()] += 1.0;
	}

	double left_total = 0;
	double right_total = 0;
	
	double left_size = left_split.size();
	double right_size = right_split.size();
	//classes
	for(int i = 0; i < 2; i++){
		left_total += left_counts[i]/left_size * log(left_counts[i]/left_size);
		right_total += right_counts[i]/right_size * log(right_counts[i]/right_size);
	}
	
	return ((left_size * left_total) + (right_size * right_total))/total_points;
}

double DRRClassSplitCalculator::calculate_current_regression_uncertainty(vector<ImagePatch>& data){
	matrix angle_cov_matrix = calculate_angle_vector_covariance_matrix(data);
	matrix offset_cov_matrix = calculate_offset_vector_covariance_matrix(data);
	
	return log2(determinant(offset_cov_matrix) + determinant(angle_cov_matrix));
	
}
double DRRClassSplitCalculator::calculate_regression_uncertainty(vector<ImagePatch>& left_split, vector<ImagePatch>& right_split){
	//total
	
	
	// left and right
	matrix left_offset_vector_cov = calculate_offset_vector_covariance_matrix(left_split);
	matrix left_angle_vector_cov = calculate_angle_vector_covariance_matrix(left_split);
	
	matrix right_offset_vector_cov = calculate_offset_vector_covariance_matrix(right_split);
	matrix right_angle_vector_cov = calculate_angle_vector_covariance_matrix(right_split);
	
	double total_patches = left_split.size() + right_split.size();
	double left_weight = ((double)left_split.size())/total_patches;
	double right_weight = ((double)right_split.size())/total_patches;

	double det_lac = determinant(left_angle_vector_cov);
	double det_loc = determinant(left_offset_vector_cov);
	double det_rac = determinant(right_angle_vector_cov);
	double det_roc = determinant(right_offset_vector_cov);
	
	double ru = (left_weight * log2(det_loc + det_lac))+ ( right_weight * log2(det_rac + det_roc));
	
	return  ru;
}



DRRClassSplitCalculator::~DRRClassSplitCalculator(){}
