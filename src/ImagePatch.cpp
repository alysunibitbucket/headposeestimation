/*
 * ImagePatch.cpp
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */

#include "ImagePatch.h"
#include <pcl/point_types.h>

using namespace pcl;
using namespace std;

ImagePatch::ImagePatch(int class_type, PointXYZ head_centre_point, float yaw, float pitch, float roll, string person_id, int depth_file_key, PointXYZ patch_centre_point)
:class_type(class_type), pitch(pitch), yaw(yaw), 
 roll(roll), person_id(person_id), depth_file_key(depth_file_key), patch_centre_point(patch_centre_point)
{
	head_centre_point_offset_from_patch_centre.x = head_centre_point.x - patch_centre_point.x;
	head_centre_point_offset_from_patch_centre.y = head_centre_point.y - patch_centre_point.y;
	head_centre_point_offset_from_patch_centre.z = head_centre_point.z - patch_centre_point.z;
	
//	cout << "head_centre offsets are " << head_centre_point_offset_from_patch_centre.x << ", " << head_centre_point_offset_from_patch_centre.y << ", " << head_centre_point_offset_from_patch_centre.y << endl;
	
//	head_centre_point_offset_from_patch_centre = head_centre_point;
}


ImagePatch::~ImagePatch() {

}
